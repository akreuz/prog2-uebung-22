#include <stdio.h>

int fib(int i) {
    if (i == 1 || i == 2) {
        return 1;
    } else {
        return fib(i-1) + fib(i - 2);
    }
}

int main(void) {
    int n;
    int res;

    printf("Geben sie sie an wie viele Fibonacci Zahlen berechnet werden sollen, n = ");
    scanf("%d", &n);

    printf("\nDie ersten %d Fibonacci Zahlen werden ausgegeben:\n", n);
    for (int i = 1; i <= n; i++) {
        res = fib(i);
        printf("fib(%d) = %d\n", i, res);    
    }
}

