#include <stdio.h>

int main(void) {
    float netto;
    float mwst;
    float brutto;
    float skonto;
    float rechnung;


    printf("Geben sie den Nettopreis an: ");
    scanf("%f", &netto);
    
    mwst = netto * 0.2;
    brutto = netto + mwst;
    skonto = brutto * 0.02;
    rechnung = brutto - skonto;

    printf("\nNettopreis               Euro %07.2f\n", netto);
    printf("+ 20%% MwSt               Euro %07.2f\n", mwst);
    printf("=====================================\n");
    printf("Bruttopreis              Euro %07.2f\n", brutto);
    printf("- 2%% Skonto              Euro %07.2f\n", skonto);
    printf("=====================================\n");
    printf("Rechnungsbetrag          Euro %07.2f\n", rechnung);
}