#include <stdio.h>
#include <limits.h>

int main(void) {
    char res;
    char minChar = -1;
    char maxChar = 1;
    short s = 0, s_old = 0;
    int i_old = 0, i = 0;
    unsigned char uc = 0, uc_old = 0;
    unsigned short us = 0, us_old = 0;
    unsigned int ui = 0, ui_old = 0;
    
    printf("Berechnet minimalen Wert von char: ");

    while (minChar - 1 < 0) {
        res = minChar;
        minChar--;
    }

    printf("%d\n", res);

    printf("Minimalwert von char aus limit.h: %d\n", SCHAR_MIN);

    printf("Berechnet maximalen Wert von char: ");

    while (maxChar + 1 > 0) {
        res = maxChar;
        maxChar++;
    }

    printf("%d\n", res);

    printf("Maximalwert von char aus limit.h: %d\n", SCHAR_MAX);

    printf("Berechnet minimalen Wert von short: ");

    while (--s < s_old)
        s_old = s;

    printf("%d\n", s_old);

    printf("Minimalwert von short aus limit.h: %d\n", SHRT_MIN);

    s = 0, s_old = 0;

    printf("Berechnet maximalen Wert von short: ");

    while (++s > s_old)
        s_old = s;
    
    printf("%d\n", s_old);  

    printf("Maximalwert von short aus limit.h: %d\n", SHRT_MAX);

    printf("Berechnet minimalen Wert von int: ");

    while (--i < i_old)
        i_old = i;
    
    printf("%d\n", i_old); 

    printf("Minimalwert von int aus limit.h: %d\n", INT_MIN);

    i_old = 0, i = 0; 

    printf("Berechnet maximalen Wert von int: ");

    while (++i > i_old)
        i_old = i;
    
    printf("%d\n", i_old);  

    printf("Maximalwert von int aus limit.h: %d\n", INT_MAX);

    printf("Berechnet minimalen Wert von unsigned char: "); 

    while (--uc < uc_old) 
        uc_old = uc;

    printf("%d\n", uc_old);

    printf("Minimalwert von unsigned char ist %d\n", 0);

    uc = 0, uc_old = 0; 

    printf("Berechnet maximalen Wert von unsigned char: "); 

    while (++uc > uc_old) 
        uc_old = uc;

    printf("%d\n", uc_old); 

    printf("Maximalwert von unsigned char aus limit.h: %d\n", UCHAR_MAX);

    printf("Berechnet minimalen Wert von unsigned short: "); 

    while(--us < us_old)
        us_old = us;

    printf("%d\n", us_old);

    printf("Minimalwert von unsigned short ist %d\n", 0);

    us = 0, us_old = 0;

    printf("Berechnet maximalen Wert von unsigned short: ");

    while(++us > us_old)
        us_old = us;

    printf("%d\n", us_old);

    printf("Maximalwert von unsigned short aus limit.h: %d\n", USHRT_MAX);

    printf("Berechnet minimalen Wert von unsigned int: ");

    while (--ui < ui_old)
        ui_old = ui;

    printf("%d\n", ui_old);

    printf("Minimalwert von unsigned int ist %d\n", 0);

    ui = 0, ui_old = 0;

    printf("Berechnet maximalen Wert von unsigned int: ");

    while (++ui > ui_old)
        ui_old = ui;
    
    printf("%d\n", ui_old);

    printf("Maximalwert von unsigned int aus limit.h: %d\n", UINT_MAX);
}